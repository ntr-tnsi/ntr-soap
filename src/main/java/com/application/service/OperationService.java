package com.application.service;

public interface OperationService {

    String doCredit(final Double amount ,final long acheteur,final long vendeur );

    String doDebit(final Double amount ,final long acheteur,final long vendeur );
}

package com.application.service.impl;

import com.application.config.ErrorMessage;
import com.application.config.SucessMessage;
import com.application.service.OperationService;
import org.apache.http.client.methods.HttpPost;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class IOperationService implements OperationService {

    @Override
    public String doDebit(final Double amount ,final long acheteur,final long vendeur ) {
        try {
            HttpPost post = new HttpPost("http://localhost:8081/account/debit/" + amount + "/" + acheteur + "/" + vendeur);

            CloseableHttpResponse response = httpClient.execute(post)) {

            String result = EntityUtils.toString(response.getEntity());
            System.err.println(result);
            return SucessMessage.PAYEMENT_SENDED;
        } catch (Exception e) {
            return ErrorMessage.ERROR_500;
        }
    }

    @Override
    public String doCredit(final Double amount ,final long acheteur,final long vendeur ) {
        try {

            HttpPost post = new HttpPost("http://localhost:8081/account/credit/" + amount + "/" + vendeur + "/" + acheteur);

            CloseableHttpResponse response = httpClient.execute(post)) {

            String result2 = EntityUtils.toString(response.getEntity());
            return SucessMessage.PAYEMENT_RECEVED;
        } catch (Exception e) {
            return ErrorMessage.ERROR_500;
        }
    }


}

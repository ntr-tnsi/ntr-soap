package com.application.controller;

import com.application.service.OperationService;
import com.application.service.impl.IOperationService;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class OperationController {

    private OperationService operationService = new IOperationService();

    public String credit(final Double amount ,final long acheteur,final long vendeur ) {
        return operationService.doCredit(amount, acheteur, vendeur);
    }

    public String debit(final Double amount ,final long acheteur,final long vendeur ) {
        return operationService.doDebit(amount, acheteur, vendeur);
    }
}
